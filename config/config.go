// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package config

import (
	"amreo/spostid-cli/utils"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/OpenPeeDeeP/xdg"
)

const (
	// NotInitialized status is when the app hasn't initialized the app session
	NotInitialized = 0
	// Initialized status is when the app has initialized but the user hasn't logged
	Initialized = 1
	// Logged status is when the the user has logged but has not superlogged
	Logged = 2
	// SuperLogged status is when the user has superlogged
	SuperLogged = 3
)

// Configuration is a struct that contains non-secret configuration parameters
type Configuration struct {
	// XDevice is a string that contains a unique identifier for the client. Its format is
	// <random 22-digit hex number>::<operating system>:<os version>:<smartphone model>:<build version>:<is rooted>
	// ex: "B97CB1FE9829308E54C8FF::Android:10:ONE A2003:4.5.204:false"
	XDevice string
	// Status contains the status of the client. The state may be NotInitialized, Initialized, Logged, and SuperLogged
	Status int
	// ServerPubKey is the key used to sign the JWE tokens sent to the server
	ServerPubKey *rsa.PublicKey `json:"-"`
	// ServerPubKeyPEM is the rappresentaton of ServerPubKey in the PEM format
	ServerPubKeyPEM string
	// AppUUID is the uuid used by the server to identify the session of the client
	AppUUID string
	// UserTOTPInterval is the interval of the TOTP token used by the user
	UserTOTPInterval int
	// PasswordRules is a slice containing the rules for the password
	PasswordRules []string
	// PasswordChangeErrors is the map containing password changing errors
	PasswordChangeErrors map[string]string
	// AppRegisterID is the ID of the user session
	AppRegisterID string
	// ProfileToken is the token that contains user profile info
	ProfileToken string
	// AccessTokenExpiresIn contains the duration of the access token
	AccessTokenExpiresIn int
	// ProxyURL is the url of the proxy. If it is not set, no proxy is used
	ProxyURL string
	// NoVerifySSL is true if the SSL verification should be disabled
	NoVerifySSL bool
	// DebugHTTP true if shows HTTP response
	DebugHTTP bool
	// NotAutoSaveis false if autosave the configuration, otherwise it is false
	NotAutoSave bool
}

// DumpConfiguration dumps to the stdout the configuration in JSON format
func (self Configuration) DumpConfiguration() {
	bytes, err := json.MarshalIndent(self, "", " ")
	utils.PanicOrNoErr(err)
	fmt.Println(string(bytes))
}

// CleanSessionData cleans the session data
func (self *Configuration) CleanSessionData() {
	self.Status = NotInitialized
	self.ServerPubKey = nil
	self.ServerPubKeyPEM = ""
	self.AppUUID = ""
	self.UserTOTPInterval = -1
	self.AppRegisterID = ""
	self.ProfileToken = ""
	self.AccessTokenExpiresIn = 0
}

// LoadConfiguration load the configuration from the $XDG_CONFIG_HOME/spostid-cli/spostid-cli.json
// and returns it as a struct.
func LoadConfiguration() Configuration {
	configDir := xdg.ConfigHome() + "/spostid-cli"
	filename := configDir + "/spostid-cli.json"
	var loadedConfig Configuration

	// Read the config file
	bytes, err := os.ReadFile(filename)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		fmt.Fprintf(os.Stderr, "La lettura del file di configurazione da %s ha fallito. Errore: %s\n", filename, err)
		return loadedConfig
	}

	// Convert the json content to the struct
	err = json.Unmarshal(bytes, &loadedConfig)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La decodifica JSON del %s ha fallito. Errore: %s\n", filename, err)
		return loadedConfig
	}

	if loadedConfig.ServerPubKeyPEM != "" {
		loadedConfig.ServerPubKey = utils.PEM2RSAPubKey(loadedConfig.ServerPubKeyPEM)
	}

	return loadedConfig
}

// Save saves the content of the configuration to the disk in $XDG_CONFIG_HOME/spostid-cli/spostid-cli.json
func (self Configuration) Save() {
	configDir := xdg.ConfigHome() + "/spostid-cli"
	filename := configDir + "/spostid-cli.json"

	// Create the config directory
	if err := os.Mkdir(configDir, 0700); err != nil && !errors.Is(err, os.ErrExist) {
		fmt.Fprintf(os.Stderr, "La creazione della directory di configurazione in %s ha fallito. Errore: %s\n", configDir, err)
		return
	}

	// Fix client status to prevent saving SuperLogged as status
	otherSelf := self
	if otherSelf.Status == SuperLogged {
		otherSelf.Status = Logged
	}

	// Convert the configuration struct to json in bytes
	bytes, err := json.MarshalIndent(otherSelf, "", " ")
	utils.PanicOrNoErr(err)

	// Open the config file
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Il salvataggio della configurazione in %s ha fallito. Errore: %s\n", filename, err)
		return
	}

	// Write the json to the config file
	if _, err := f.Write(bytes); err != nil {
		fmt.Fprintf(os.Stderr, "Il salvataggio della configurazione in %s ha fallito. Errore: %s\n", filename, err)
		return
	}
}

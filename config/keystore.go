// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package config

import (
	"amreo/spostid-cli/utils"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"os"
	"reflect"

	"github.com/zalando/go-keyring"
)

// Secrets is a struct containing secret informations
type Secrets struct {
	// AppPrivKey is the key used to sign the JWEs to be sent to the backends
	AppPrivKey *rsa.PrivateKey `saver:"nosaved" json:"-"`
	// AppPrivKeyPEM is the rappresentation of AppPrivKey in the PEM format
	AppPrivKeyPEM string
	// AppHOTPSecretKey contains the secret key used to generate the OtpSpec(s) for the JWE requests
	AppHOTPSecretKey string
	// RegisterToken is the token used to register the session for operations
	RegisterToken string
	// SecretAppToken is the token used to sign challenges for superuser operations
	SecretAppToken string
	// AccessToken is the token used to perform superuser operations
	AccessToken string `saver:"nosaved"`
	// PosteIDCode contains the cached PosteID code
	PosteIDCode string `saver:"nosaved"`
}

// DumpSecrets dumps to the stdout the secrets in JSON format
func (self Secrets) DumpSecrets() {
	bytes, err := json.MarshalIndent(self, "", " ")
	utils.PanicOrNoErr(err)
	fmt.Println(string(bytes))
}

// CleanSessionData cleans the session data
func (self *Secrets) CleanSessionData() {
	self.AppPrivKeyPEM = ""
	self.AppPrivKey = nil
	self.AppHOTPSecretKey = ""
	self.RegisterToken = ""
	self.SecretAppToken = ""
	self.AccessToken = ""
	self.PosteIDCode = ""
}

// LoadSecrets loads the secrets from the operating system keyring and returns it
// On GNU/Linux the keyring could be viewed using seahorse or gnome secrets service o similar software
func LoadSecrets() Secrets {
	var loadedSecrets Secrets
	service := "spostid-cli"

	// using reflection, loop all struct fields
	v := reflect.ValueOf(&loadedSecrets).Elem()
	for i := 0; i < v.NumField(); i++ {
		// extract field info
		f := v.Field(i)
		fieldName := v.Type().Field(i).Name // Is the key of the "password"

		if v.Type().Field(i).Tag.Get("saver") == "nosaved" {
			continue
		}

		// Save the password for spostid-cli with the key fieldname and the value of the field
		value, err := keyring.Get(service, fieldName)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Il caricamento dei segreti per il campo %s ha fallito. Ignorato. Errore: %s\n", fieldName, err)
			continue
		}

		// Set the value to the struct field
		switch f.Kind() {
		case reflect.String:
			f.SetString(value)
		default:
			panic("Not supported field type for " + fieldName)
		}
	}

	if loadedSecrets.AppPrivKeyPEM != "" {
		loadedSecrets.AppPrivKey = utils.PEM2RSAPrivKey(loadedSecrets.AppPrivKeyPEM)
	}

	return loadedSecrets
}

// Save saves the content of the secrets to the operating system keyring
// On GNU/Linux the keyring could be viewed using seahorse or gnome secrets service o similar software
func (self Secrets) Save() {
	service := "spostid-cli"

	// using reflection, loop all struct fields
	v := reflect.ValueOf(self)
	for i := 0; i < v.NumField(); i++ {
		// extract field info
		f := v.Field(i)
		fieldName := v.Type().Field(i).Name // Is the key of the "password"

		if v.Type().Field(i).Tag.Get("saver") == "nosaved" {
			continue
		}

		switch f.Kind() {
		case reflect.String:
			// Save the password for spostid-cli with the key fieldname and the value of the field
			if err := keyring.Set(service, fieldName, f.String()); err != nil {
				fmt.Fprintf(os.Stderr, "Il salvataggio dei segreti per il campo %s ha fallito. Errore: %s\n", fieldName, err)
				return
			}
		default:
			panic("Not supported field type for " + fieldName)
		}
	}
}

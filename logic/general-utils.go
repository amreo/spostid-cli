// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package logic

import (
	"amreo/spostid-cli/config"
	"amreo/spostid-cli/utils"
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gopkg.in/square/go-jose.v2"
)

const AppName = "app-posteid-v3"
const JWEExpirePeriod = 60

var emptyMap struct{}

// GenHOTPOtpSpecs generates a new HOTP token and returns it as a OtpSpec
// secretKey is the key used to generate the token
func GenHOTPOtpSpecs(secretKey string) interface{} {
	token, mf := utils.HOTP(secretKey)

	return map[string]interface{}{
		"movingFactor": mf,
		"otp":          token,
		"type":         "HMAC-SHA1",
	}
}

// GenHOTPXKey returns a auth key in the format $clientUUID:$toekn:$mf
// clientUUID is the clientUUID
// secretKey is the key used to generate the token
func GenHOTPXKey(clientUUID, secretKey string) string {
	token, mf := utils.HOTP(secretKey)

	return fmt.Sprintf("%s:%s:%d", clientUUID, token, mf)
}

// CalculateKidSHA256 calculate the base64-encoded(with padding) sha256 of kid and returns it as string
// kid is a string to be checksummed and base64-encoded
func CalculateKidSHA256(kid string) string {
	kidSha256 := sha256.Sum256([]byte(kid))
	return base64.RawStdEncoding.WithPadding('=').EncodeToString(kidSha256[:])
}

// GenJWEPayload returns a new jwe encrypted payload
// jti is the jti of the token. If the parameter is not specified, a random uuid is used
// sub is the sub of the token and it is usually used as verb.
// config is used to get pubServerKey used to encrypt the JWE token
// data is inserted in the claim to the data field
// It returns a encrypted json web encrypted token containing the provided informations
func GenJWEPayload(jti string, sub string, config *config.Configuration, data interface{}) string {
	now := time.Now()

	// If the jti is not set, a random uuid is set
	if jti == "" {
		jti = uuid.NewString()
	}

	// Build the claimSet
	claimSet := map[string]interface{}{
		"iss":  AppName,
		"sub":  sub,
		"jti":  jti,
		"exp":  now.AddDate(0, 0, JWEExpirePeriod).Unix(),
		"nbf":  now.Unix(),
		"iat":  now.Unix(),
		"data": data,
	}

	// Create a new encrypter that encrypt using server pub key in config.ServerPubKey
	enc, err := jose.NewEncrypter(jose.A256CBC_HS512, jose.Recipient{
		Algorithm: jose.RSA_OAEP_256,
		Key:       config.ServerPubKey,
	}, &jose.EncrypterOptions{
		ExtraHeaders: map[jose.HeaderKey]interface{}{
			"cty": "JWE",
			"typ": "JWT",
		},
	})
	utils.PanicOrNoErr(err)

	// Convert the claimset to JSON and store it in payload
	payload, err := json.Marshal(claimSet)
	utils.PanicOrNoErr(err)

	// Encrypt and serialize the payload
	jwe, err := enc.Encrypt(payload)
	utils.PanicOrNoErr(err)
	jweString, err := jwe.CompactSerialize()
	utils.PanicOrNoErr(err)

	//Returns the encrypteed token
	return jweString
}

// GenJWEPayloadAES returns a new jwe encrypted payload using PosteID AES keu
// jti is the jti of the token. If the parameter is not specified, a random uuid is used
// sub is the sub of the token and it is usually used as verb.
// data is inserted in the claim to the data field
// It returns a encrypted json web encrypted token containing the provided informations
func GenJWEPayloadAES(jti string, sub string, data interface{}) string {
	var key string = "Y0+/zLadedtiuCanDnXmbfPaWmTakm6U/jXbqvUegkOx24wgCmArTw=="
	_keySha256 := sha256.Sum256([]byte(key))
	keySha256 := _keySha256[:]

	now := time.Now()

	// If the jti is not set, a random uuid is set
	if jti == "" {
		jti = uuid.NewString()
	}

	// Build the claimSet
	claimSet := map[string]interface{}{
		"iss":  AppName,
		"sub":  sub,
		"jti":  jti,
		"exp":  now.AddDate(0, 0, JWEExpirePeriod).Unix(),
		"nbf":  now.Unix(),
		"iat":  now.Unix(),
		"data": data,
	}

	// Create a new encrypter that encrypt using shared public key
	enc, err := jose.NewEncrypter(jose.A256CBC_HS512, jose.Recipient{
		Algorithm: jose.A256KW,
		Key:       keySha256,
		KeyID:     "app-posteid-v3",
	}, &jose.EncrypterOptions{
		ExtraHeaders: map[jose.HeaderKey]interface{}{
			"cty": "JWE",
			"typ": "JWT",
		},
	})
	utils.PanicOrNoErr(err)

	// Convert the claimset to JSON and store it in payload
	payload, err := json.Marshal(claimSet)
	utils.PanicOrNoErr(err)

	// Encrypt and serialize the payload
	jwe, err := enc.Encrypt(payload)
	utils.PanicOrNoErr(err)
	jweString, err := jwe.CompactSerialize()
	utils.PanicOrNoErr(err)

	//Returns the encrypteed token
	return jweString
}

// GenJWEPayloadWithKIDOTpSpect returns a new jwe encrypted payload
// jti is the jti of the token. If the parameter is not specified, a random uuid is used
// sub is the sub of the token and it is usually used as verb.
// config is used to get pubServerKey used to encrypt the JWE token
// secrets is used to genereate the otp specs
// data is inserted in the claim to the data field
// It returns a encrypted json web encrypted token containing the provided informations
func GenJWEPayloadWithKIDOTpSpect(jti string, sub string, config *config.Configuration, secrets *config.Secrets, data interface{}) string {
	now := time.Now()

	// If the jti is not set, a random uuid is set
	if jti == "" {
		jti = uuid.NewString()
	}

	// Build the claimSet
	claimSet := map[string]interface{}{
		"iss":        AppName,
		"sub":        sub,
		"jti":        jti,
		"exp":        now.AddDate(0, 0, JWEExpirePeriod).Unix(),
		"nbf":        now.Unix(),
		"iat":        now.Unix(),
		"data":       data,
		"otp-specs":  GenHOTPOtpSpecs(secrets.AppHOTPSecretKey),
		"kid-sha256": CalculateKidSHA256(config.AppUUID),
	}

	// Create a new encrypter that encrypt using server pub key in config.ServerPubKey
	enc, err := jose.NewEncrypter(jose.A256CBC_HS512, jose.Recipient{
		Algorithm: jose.RSA_OAEP_256,
		Key:       config.ServerPubKey,
		KeyID:     config.AppUUID,
	}, &jose.EncrypterOptions{
		ExtraHeaders: map[jose.HeaderKey]interface{}{
			"cty": "JWE",
		},
	})
	utils.PanicOrNoErr(err)

	// Convert the claimset to JSON and store it in payload
	payload, err := json.Marshal(claimSet)
	utils.PanicOrNoErr(err)

	// Encrypt and serialize the payload
	jwe, err := enc.Encrypt(payload)
	utils.PanicOrNoErr(err)
	jweString, err := jwe.CompactSerialize()
	utils.PanicOrNoErr(err)

	//Returns the encrypteed token
	return jweString
}

// DecryptJWEPayload decrypts the payload in jweIn and set it to out
// secret is used to decrypt jweIn
// jweIn is the input payload and a encrypted jwe token
// out is where the output is set
func DecryptJWEPayload(secret *config.Secrets, jweIn string, out interface{}) error {
	// Parse and decrypt the payload
	object, err := jose.ParseEncrypted(jweIn)
	if err != nil {
		return err
	}
	decryptedPayload, err := object.Decrypt(secret.AppPrivKey)
	if err != nil {
		return err
	}

	// Unmarshal decryptedPayload in JSON format and set the value to out
	if err = json.Unmarshal(decryptedPayload, out); err != nil {
		return err
	}

	return nil
}

// DecryptJWEPayloadAES decrypts the payload in jweIn and set it to out
// secret is used to decrypt jweIn
// jweIn is the input payload and a encrypted jwe token
// out is where the output is set
func DecryptJWEPayloadAES(jweIn string, out interface{}) error {
	var key string = "Y0+/zLadedtiuCanDnXmbfPaWmTakm6U/jXbqvUegkOx24wgCmArTw=="
	_keySha256 := sha256.Sum256([]byte(key))
	keySha256 := _keySha256[:]

	// Parse and decrypt the payload
	object, err := jose.ParseEncrypted(jweIn)
	if err != nil {
		return err
	}
	decryptedPayload, err := object.Decrypt(keySha256)
	if err != nil {
		return err
	}

	// Unmarshal decryptedPayload in JSON format and set the value to out
	if err = json.Unmarshal(decryptedPayload, out); err != nil {
		return err
	}

	return nil
}

// GenErrorFromCmdHeader builds a error from the command header info and returns it
// cmdHeader is the input CommandHeader
func GenErrorFromCmdHeader(cmdHeader CommandHeader) error {
	return fmt.Errorf("error from backend: result: %s, reason: %s, description: %s, details: %s",
		cmdHeader.CommandResult, cmdHeader.CommandResultReason, cmdHeader.CommandResultDescription, cmdHeader.CommandResultDetails,
	)
}

// GenErrorFromCmdResult builds a error from the command result info and returns it
// cmdResult is the input CommandResult
func GenErrorFromCmdResult(cmdResult CommandResult) error {
	return fmt.Errorf("error from backend: errorCode: %s, description: %s",
		cmdResult.CommandErrorCode, cmdResult.CommandErrorMessage,
	)
}

// SignChallenge sign the challenge using the specified parameters and returns the signature
func SignChallenge(secretAPP string, posteIDPin string, challenge TransactionChallenge) string {
	key := secretAPP + posteIDPin + challenge.RandK
	hmac := hmac.New(sha256.New, []byte(key))
	hmac.Write([]byte(challenge.TransactionChallenge))
	hash := hmac.Sum([]byte{})
	return base64.RawURLEncoding.WithPadding(base64.NoPadding).EncodeToString(hash)
}

// GenerateInitCodeVerifier returns a pair of a uuid, and its base64-encoded (with padding) sha256 checksum
// The first returned values is the uuid, the second value is its base64-encoded (with padding) sha256 checksum
func GenerateInitCodeVerifier() (string, string) {
	initCodeVerifierUUID := uuid.New().String()
	initCodeVerifierUUIDChecksum := sha256.Sum256([]byte(initCodeVerifierUUID))
	initCodeVerifier := base64.RawStdEncoding.WithPadding('=').EncodeToString(initCodeVerifierUUIDChecksum[:])
	return initCodeVerifierUUID, initCodeVerifier
}

// ShowHTTPResponseIfDebug shows the response if DebugHTTP is enabled
func ShowHTTPResponseIfDebug(config *config.Configuration, resp *http.Response) {
	if config.DebugHTTP {
		respBody, err := ioutil.ReadAll(resp.Body)
		utils.PanicOrNoErr(err)

		utils.DumpHTTPResponseWithBodyString(*resp, string(respBody))
		resp.Body = ioutil.NopCloser(bytes.NewBuffer(respBody))
	}
}

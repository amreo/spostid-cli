// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package logic

import (
	"amreo/spostid-cli/config"
	"amreo/spostid-cli/utils"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// InitSession initialiaze the client session, and set the status to config.Initialized
func InitSession(conf *config.Configuration, secret *config.Secrets) {
	conf.Status = config.NotInitialized

	// Generate the client RSA Key Pair
	clientPrivKey, clientPrivKeyPEM, clientPubKey := utils.GenerateRSAKeyPair()
	secret.AppPrivKey = clientPrivKey
	secret.AppPrivKeyPEM = clientPrivKeyPEM

	// Generate a init code verifier/identifier for pub key exchange
	initCodeVerifierUUID, initCodeVerifier := GenerateInitCodeVerifier()

	// Fetch the server public key certificate given the initCodeVerifier
	resp, err := AppRegistryRegisterInit(conf, initCodeVerifier)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta della chiave pubblica del server ha fallito. Errore: %s\n", err)
		return
	} else if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della chiave pubblica del server ha fallito perché il server ha restituito %d\n", resp.StatusCode)
		return
	}
	conf.ServerPubKey = resp.PubKey
	conf.ServerPubKeyPEM = resp.PubKeyPEM

	// Send the client public key
	resp2, err := AppRegistryRegister(conf, secret, resp.Cookies, initCodeVerifierUUID, clientPubKey)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'invio della chiave pubblica ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'invio della chiave pubblica ha fallito ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	}

	conf.AppUUID = resp2.Data.AppUUID
	secret.AppHOTPSecretKey = resp2.Data.OtpSecretKey

	// Activate the client
	resp3, err := AppRegistryActivation(conf, secret, resp2.Cookies)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'attivazione ha fallito. Errore: %s\n", err)
		return
	} else if resp3.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'attivazione ha fallito perché il server ha restituito %d\n", resp3.StatusCode)
		return
	}

	// Fetch che client config
	resp4, err := AppRegistryAppConfig(conf, secret)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'ottenimento della configurazione ha fallito. Errore: %s\n", err)
		return
	} else if resp4.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'ottenimento della configurazione ha fallito perché il server ha restituito %d\n", resp4.StatusCode)
		return
	} else if resp4.Header.CommandResult != "0" {
		fmt.Fprintf(os.Stderr, "L'ottenimento della configurazione ha fallito. Errore: %s\n", GenErrorFromCmdHeader(resp4.Header))
		return
	}
	conf.UserTOTPInterval, err = strconv.Atoi(resp4.Body.Config.TokenIntervalSeconds)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'ottenimento della configurazione ha fallito. Errore: %s\n", err)
		return
	}
	conf.PasswordRules = resp4.Body.Config.PasswordRules.Rules
	conf.PasswordChangeErrors = resp4.Body.Config.PasswordChangeErrors
	if resp4.Body.Config.Version != "1" {
		fmt.Fprintf(os.Stderr, "La versione indicata dalla configurazione ottenuta è diversa da 1 ciò potrebbe comportare problemi. Ignorata")
	}

	// Call the app check api
	resp5, err := AppRegistryAppCheck(conf, secret)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato). Errore: %s\n", err)
	} else if resp5.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato) perché il server ha restituito %d\n", resp5.StatusCode)
	} else if resp5.Header.CommandResult != "0" {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato). Errore: %s\n", GenErrorFromCmdHeader(resp5.Header))
	}

	// Update the status
	conf.Status = config.Initialized
}

// AppRegistryRegisterInit fetch the pub server key from the server
// It calls the api https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/registerInit
func AppRegistryRegisterInit(config *config.Configuration, initCodeVerifier string) (AppRegistryRegisterInitResponse, error) {
	var outResp AppRegistryRegisterInitResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2.1/registerInit"

	jwe := GenJWEPayloadAES("", "registerInit", map[string]interface{}{
		"initCodeChallenge": initCodeVerifier,
	})

	req, err := http.NewRequest("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	utils.PanicOrNoErr(err)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)

	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if resp.StatusCode == 200 {
		if err = DecryptJWEPayloadAES(utils.IOReaderToString(resp.Body), &outResp); err != nil {
			return outResp, err
		}

		outResp.PubKeyPEM = "-----BEGIN PUBLIC KEY-----\n" + outResp.Data.PubServerKey + "\n-----END PUBLIC KEY-----"
		outResp.PubKey = utils.PEM2RSAPubKey(outResp.PubKeyPEM)
	}

	return outResp, nil
}

// AppRegistryRegister send to the server the client pub key
// It calls the api https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/register
func AppRegistryRegister(config *config.Configuration, secret *config.Secrets, cookies []*http.Cookie, initCodeVerifierUUID string, clientPubKey string) (AppRegistryRegisterResponse, error) {
	var outResp AppRegistryRegisterResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/register"
	jwe := GenJWEPayload("", "register", config, map[string]interface{}{
		"initCodeVerifier": initCodeVerifierUUID,
		"xdevice":          config.XDevice,
		"pubAppKey":        utils.StripFirstLastLine(clientPubKey),
	})

	req, err := http.NewRequest("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	utils.PanicOrNoErr(err)
	utils.AddCookiesToRequest(req, cookies)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)

	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if resp.StatusCode == 200 {
		if err = DecryptJWEPayload(secret, utils.IOReaderToString(resp.Body), &outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

// AppRegistryActivation activate the client
// It calls the api https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/activation
func AppRegistryActivation(config *config.Configuration, secret *config.Secrets, cookies []*http.Cookie) (AppRegistryActivationResponse, error) {
	var outResp AppRegistryActivationResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/activation"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "activation", config, secret, emptyMap)

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	utils.AddCookiesToRequest(req, cookies)
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	return outResp, nil
}

// AppRegistryAppConfig fetch che configuration of the client
// It calls the API https://appregistry-posteid.mobile.poste.it/jod-app-registry/v1/appregistry/appconfig
func AppRegistryAppConfig(config *config.Configuration, secret *config.Secrets) (AppRegistryAppConfigResponse, error) {
	var outResp AppRegistryAppConfigResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v1/appregistry/appconfig"
	body := map[string]interface{}{
		"header": emptyMap,
		"body":   emptyMap,
	}

	req := utils.NewRequestOrPanic("POST", endpoint, utils.ToJSONReader(body))
	req.Header.Set("X-KEY", GenHOTPXKey(config.AppUUID, secret.AppHOTPSecretKey))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// AppRegistryAppCheck check the client
// It calls the API https://appregistry-posteid.mobile.poste.it/jod-app-registry/v1/appregistry/appcheck
func AppRegistryAppCheck(config *config.Configuration, secret *config.Secrets) (AppRegistryAppCheckResponse, error) {
	var outResp AppRegistryAppCheckResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v1/appregistry/appcheck"
	body := map[string]interface{}{
		"header": emptyMap,
		"body":   emptyMap,
	}

	req := utils.NewRequestOrPanic("POST", endpoint, utils.ToJSONReader(body))
	req.Header.Set("X-KEY", GenHOTPXKey(config.AppUUID, secret.AppHOTPSecretKey))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// UpdateSession updates the client session
func UpdateSession(conf *config.Configuration, secret *config.Secrets) {
	if conf.Status < config.Initialized {
		fmt.Fprintf(os.Stderr, "Devi aver già inizializzato spostid-cli per aggiornare la sessione. Esegui il comando init")
		return
	}

	// Generate a init code verifier/identifier for pub key exchange
	initCodeVerifierUUID, initCodeVerifier := GenerateInitCodeVerifier()

	// Fetch the server public key certificate given the initCodeVerifier
	resp, err := AppRegistryRegisterInit(conf, initCodeVerifier)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta della chiave pubblica del server ha fallito. Errore: %s\n", err)
		return
	} else if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della chiave pubblica del server ha fallito perché il server ha restituito %d\n", resp.StatusCode)
		return
	}
	conf.ServerPubKey = resp.PubKey
	conf.ServerPubKeyPEM = resp.PubKeyPEM

	// Send the client public key
	resp2, err := AppRegistryRegisterUpdate(conf, secret, resp.Cookies, initCodeVerifierUUID)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'invio della chiave pubblica ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'invio della chiave pubblica ha fallito ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	}

	conf.AppUUID = resp2.Data.AppUUID
	secret.AppHOTPSecretKey = resp2.Data.OtpSecretKey

	// Activate the client
	resp3, err := AppRegistryActivation(conf, secret, resp2.Cookies)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'attivazione ha fallito. Errore: %s\n", err)
		return
	} else if resp3.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'attivazione ha fallito perché il server ha restituito %d\n", resp3.StatusCode)
		return
	}

	// Call the app check api
	resp5, err := AppRegistryAppCheck(conf, secret)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato). Errore: %s\n", err)
	} else if resp5.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato) perché il server ha restituito %d\n", resp5.StatusCode)
	} else if resp5.Header.CommandResult != "0" {
		fmt.Fprintf(os.Stderr, "Il controllo del client ha fallito (ma ignorato). Errore: %s\n", GenErrorFromCmdHeader(resp5.Header))
	}

	// Update the status
	conf.Status = config.Initialized
}

// AppRegistryRegister send to the server the client pub key
// It calls the api https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/register
func AppRegistryRegisterUpdate(config *config.Configuration, secret *config.Secrets, cookies []*http.Cookie, initCodeVerifierUUID string) (AppRegistryRegisterResponse, error) {
	var outResp AppRegistryRegisterResponse
	endpoint := "https://appregistry-posteid.mobile.poste.it/jod-app-registry/v2/register"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "register", config, secret, map[string]interface{}{
		"initCodeVerifier": initCodeVerifierUUID,
		"xdevice":          config.XDevice,
		"pubAppKey":        utils.StripFirstLastLine(utils.RSAPubKey2PEM(&secret.AppPrivKey.PublicKey)),
	})

	req, err := http.NewRequest("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	utils.PanicOrNoErr(err)
	utils.AddCookiesToRequest(req, cookies)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)

	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if resp.StatusCode == 200 {
		if err = DecryptJWEPayload(secret, utils.IOReaderToString(resp.Body), &outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

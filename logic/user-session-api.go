// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package logic

import (
	"amreo/spostid-cli/config"
	"amreo/spostid-cli/utils"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"syscall"

	"github.com/google/uuid"
	"golang.org/x/term"
)

// Login logins the user by asking the username and the password
func Login(conf *config.Configuration, secret *config.Secrets) {
	// Check the configuration status, and if it is not initialized, return
	if conf.Status < config.Initialized {
		fmt.Fprintf(os.Stderr, "Devi aver già inizializzato spostid-cli per fare il login.\n Esegui il comando init")
		return
	}

	username := utils.AskString("Inserire l'username/indirizzo email: ")

	// Ask the password
	fmt.Print("Inserire la password: ")
	bytePassword, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'inserimento della password  ha fallito. Errore: %s\n", err)
		return
	}
	fmt.Println()

	// Send the username/password key pair
	username = strings.TrimSpace(username)
	password := strings.TrimSpace(string(bytePassword))
	resp1, err := SecureLoginXMobileAuth0(conf, secret, username, password)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta di login(prima fase) ha fallito. Errore: %s\n", err)
		return
	} else if resp1.StatusCode == 401 {
		fmt.Fprintln(os.Stderr, "Le credenziali inserite sono sbagliate")
		return
	} else if resp1.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta di login(prima fase) ha fallito perché il server ha restituito %d\n", resp1.StatusCode)
		return
	} else if resp1.Pi == "" {
		fmt.Fprintf(os.Stderr, "X-PI assente. C'è stato qualche errore nella richiesta")
		return
	}

	// Send request to send the SMS token
	resp2, err := SecureLoginXMobileAuth3(conf, secret, resp1.Cookies, username, password)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'invio della richiesta di token ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'invio della richiesta di token ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	} else if resp2.Tel == "" {
		fmt.Fprintln(os.Stderr, "X-TEL assente. C'è stato qualche errore nella richiesta")
		return
	}

	// Ask the received SMS code/pin
	pin := utils.AskString(fmt.Sprintf("Inserire codice ricevuto via SMS al numero di cellulare ********%s: ", resp2.Tel))
	pin = strings.TrimSpace(pin)

	// Send the received sms code and complete the login
	resp3, err := SecureLoginXMobileAuth2(conf, secret, resp1.Cookies, pin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La conferma del login ha fallito. Errore: %s\n", err)
		return
	} else if resp3.StatusCode == 401 {
		fmt.Fprintln(os.Stderr, "Il pin inserito è errato")
		return
	} else if resp3.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La conferma del login ha fallito perché il server ha restituito %d\n", resp3.StatusCode)
		return
	} else if resp3.Pi == "" {
		fmt.Fprintln(os.Stderr, "X-PI assente. C'è stato qualche errore nella richiesta")
		return
	}
	secret.RegisterToken = resp3.Data.RegisterToken

	// Greeting
	fmt.Println("Benvenuto ", resp3.Pi)

	repeat := true

	for repeat {
		// Request for another user session credentials
		resp4, err := SecureHolderRegisterApp(conf, secret, secret.PosteIDCode)
		if err != nil {
			fmt.Fprintf(os.Stderr, "La registrazione del client ha fallito. Errore: %s\n", err)
			return
		} else if resp4.CommandErrorCode == "PIN-ERR-1" {
			repeat = true
			fmt.Print("Inserire il codice posteid: ")
			bytePosteIDCode, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Fprintf(os.Stderr, "L'inserimento del codice posteid ha fallito. Errore: %s\n", err)
				return
			}
			fmt.Println()
			secret.PosteIDCode = strings.TrimSpace(string(bytePosteIDCode))
		} else if resp4.StatusCode != 200 {
			fmt.Fprintf(os.Stderr, "La registrazione del client ha fallito perché il server ha restituito %d\n", resp4.StatusCode)
			return
		} else if !resp4.CommandSuccess {
			fmt.Fprintf(os.Stderr, "La registrazione del client ha fallito. Errore: %s\n", GenErrorFromCmdResult(resp4.CommandResult))
			return
		} else {
			repeat = false
			conf.AppRegisterID = resp4.Data.AppRegisterID
			secret.SecretAppToken = resp4.Data.SecretApp
		}
	}

	// Update the status to logged
	conf.Status = config.Logged
}

// SecureLoginXMobileAuth0 logins the user in the zero-th auth level
// It calls the api https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt
func SecureLoginXMobileAuth0(config *config.Configuration, secret *config.Secrets, username string, password string) (SecureLoginXMobileAuthJWT0Response, error) {
	var outResp SecureLoginXMobileAuthJWT0Response
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"userid":    username,
		"password":  password,
		"authLevel": "0",
	})

	req := utils.NewRequestOrPanic("GET", endpoint, nil)
	req.Header.Set("Authorization", "Bearer "+jwe)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	outResp.Pi = resp.Header.Get("X-Pi")
	outResp.Pid, _ = utils.Base64Decode(resp.Header.Get("X-Pid"))

	return outResp, nil
}

// SecureLoginXMobileAuth3 ask the server to send a temporary SMS pin code
// It calls the api https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt
func SecureLoginXMobileAuth3(config *config.Configuration, secret *config.Secrets, cookies []*http.Cookie, username string, password string) (SecureLoginXMobileAuthJWT3Response, error) {
	var outResp SecureLoginXMobileAuthJWT3Response
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"userid":    username,
		"password":  password,
		"authLevel": "3",
	})

	req := utils.NewRequestOrPanic("GET", endpoint, nil)
	utils.AddCookiesToRequest(req, cookies)
	req.Header.Set("Authorization", "Bearer "+jwe)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()
	outResp.Tel = resp.Header.Get("X-Tel")

	return outResp, nil
}

// SecureLoginXMobileAuth2 completes the login by sending the received SMS pin code
// It calls the api https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt
func SecureLoginXMobileAuth2(config *config.Configuration, secret *config.Secrets, cookies []*http.Cookie, token string) (SecureLoginXMobileAuthJWT2Response, error) {
	var outResp SecureLoginXMobileAuthJWT2Response
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/v4/xmobileauthjwt"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"otp":       token,
		"nonce":     uuid.NewString(),
		"authLevel": "2",
	})

	req := utils.NewRequestOrPanic("GET", endpoint, nil)
	utils.AddCookiesToRequest(req, cookies)
	req.Header.Set("Authorization", "Bearer "+jwe)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()
	outResp.Pi = resp.Header.Get("X-Pi")
	outResp.Pid, _ = utils.Base64Decode(resp.Header.Get("X-Pid"))

	if outResp.StatusCode == 200 {
		if err = DecryptJWEPayload(secret, resp.Header.Get("X-Result"), &outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

// SecureHolderRegisterApp completes the login by sending the received SMS pin code
// It calls the api https://sh2-web-posteid.poste.it/jod-secure-holder2-web/public/app/v1/registerApp
func SecureHolderRegisterApp(config *config.Configuration, secret *config.Secrets, userPin string) (SecureHolderRegisterAppResponse, error) {
	var outResp SecureHolderRegisterAppResponse
	endpoint := "https://sh2-web-posteid.poste.it/jod-secure-holder2-web/public/app/v1/registerApp"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "registerApp", config, secret, map[string]interface{}{
		"userPIN":        userPin,
		"idpAccessToken": "",
		"registerToken":  secret.RegisterToken,
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	if outResp.CommandSuccess {
		if err = DecryptJWEPayload(secret, outResp.CommandResult.CommandResult.(string), &outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

// SecureHolderCheckRegisterApp checks the registration of the client
// It calls the api https://sh2-web-posteid.poste.it/jod-secure-holder2-web/public/app/v1/checkRegisterApp
func SecureHolderCheckRegisterApp(config *config.Configuration, secret *config.Secrets) (SecureHolderCheckRegisterAppResponse, error) {
	var outResp SecureHolderCheckRegisterAppResponse
	endpoint := "https://sh2-web-posteid.poste.it/jod-secure-holder2-web/public/app/v1/checkRegisterApp"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "checkRegisterApp", config, secret, map[string]interface{}{
		"appRegisterID": config.AppRegisterID,
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	if outResp.CommandSuccess {
		outResp.Valid = outResp.CommandResult.CommandResult.(map[string]interface{})["valid"].(bool)
	}

	return outResp, nil
}

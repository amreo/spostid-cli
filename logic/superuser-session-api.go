// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package logic

import (
	"amreo/spostid-cli/config"
	"amreo/spostid-cli/utils"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"syscall"

	"golang.org/x/term"
	"gopkg.in/square/go-jose.v2/jwt"
)

// SuperLogin logins the user to the privileged session
func SuperLogin(conf *config.Configuration, secret *config.Secrets) {
	// check the configuration status, and if it is not logged, return
	if conf.Status < config.Logged {
		fmt.Fprintf(os.Stderr, "Devi aver fatto il login per fare il super login.\n Esegui il comando login")
		return
	}

	if secret.PosteIDCode == "" {
		// Ask the posteid code
		fmt.Print("Inserire il codice posteid: ")
		bytePosteIDCode, err := term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			fmt.Fprintf(os.Stderr, "L'inserimento del codice posteid ha fallito. Errore: %s\n", err)
			return
		}
		fmt.Println()
		secret.PosteIDCode = strings.TrimSpace(string(bytePosteIDCode))
	}

	// Request the super-login challenge
	resp1, err := SecureHolderNativeChallenge(conf, secret)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp1.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito perché il server ha restituito %d\n", resp1.StatusCode)
		return
	}

	// Confirm the challenge
	resp2, err := SecureHolderNativeAz(conf, secret, resp1.Transaction)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "PIN-ERR-3" {
		fmt.Fprintln(os.Stderr, "Il codice PosteID inserito è errato")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "CERT-ERR-2" {
		fmt.Fprintln(os.Stderr, "Il codice PosteID bloccato per troppi errori")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status != "v4_success" {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito con status %s, ragione %s\n", resp2.Status, resp2.Reason)
		return
	}
	conf.ProfileToken = resp2.ProfileToken
	secret.AccessToken = resp2.AccessToken
	conf.AccessTokenExpiresIn = resp2.ExpiresIn

	// Update the status to super logged
	conf.Status = config.SuperLogged
}

// SecureHolderNativeChallenge requests a login challenge
// It calls the API https://posteid.poste.it/jod-securelogin-schema/secureholder/v4/native/challenge
func SecureHolderNativeChallenge(config *config.Configuration, secret *config.Secrets) (SecureHolderNativeChallengeResponse, error) {
	var outResp SecureHolderNativeChallengeResponse
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/native/v5/challenge"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, emptyMap)

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")
	req.Header.Set("User-Agent", "okhttp/3.12.1")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp.Transaction); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// SecureHolderNativeAz signs the challenge and send the signature
// It calls the API https://posteid.poste.it/jod-securelogin-schema/secureholder/v4/native/az
func SecureHolderNativeAz(config *config.Configuration, secret *config.Secrets, challenge TransactionChallenge) (SecureHolderNativeAzResponse, error) {
	var outResp SecureHolderNativeAzResponse
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/native/v5/az"
	signature := SignChallenge(secret.SecretAppToken, secret.PosteIDCode, challenge)
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"appRegisterID": config.AppRegisterID,
		"signature": utils.ToJSONWithIdent(map[string]interface{}{
			"jti":       challenge.Jti,
			"signature": signature,
			"userpin":   secret.PosteIDCode,
			"authzTool": "POSTEID",
		}),
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// ListTransactions returns the list of the transactions
// It calls the API https://posteid.poste.it/jod-securelogin-schema/secureholder/v4/native/list-transaction
func ListTransactions(config *config.Configuration, secret *config.Secrets) (ListTransactionsResponse, error) {
	var outResp ListTransactionsResponse
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/native/v5/list-transaction"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, emptyMap)

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Authorization", "Bearer "+secret.AccessToken)
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if outResp.StatusCode == 200 {
		if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
			return outResp, err
		}

		for i, tx := range outResp.Transaction.History {
			if err = json.Unmarshal([]byte(tx.AppData), &outResp.Transaction.History[i].AppDataDecoded); err != nil {
				return outResp, err
			}
		}

		for i, tx := range outResp.Transaction.Pending {
			if err = json.Unmarshal([]byte(tx.AppData), &outResp.Transaction.Pending[i].AppDataDecoded); err != nil {
				return outResp, err
			}
		}
	}

	return outResp, nil
}

// AcceptTransaction accepts the specified transaction
func AcceptTransaction(conf *config.Configuration, secret *config.Secrets, txid string) {
	// Ask the posteid code
	if secret.PosteIDCode == "" {
		fmt.Print("Inserire il codice posteid: ")
		bytePosteIDCode, err := term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			fmt.Fprintf(os.Stderr, "L'inserimento del codice posteid ha fallito. Errore: %s\n", err)
			return
		}
		fmt.Println()
		secret.PosteIDCode = strings.TrimSpace(string(bytePosteIDCode))
	}

	// Request the challenge
	resp1, err := SecureHolderChallenge(conf, secret, txid)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp1.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito perché il server ha restituito %d\n", resp1.StatusCode)
		return
	} else if resp1.Transaction.Status == "v4_error" && resp1.Transaction.Reason == "v4_invalid_request_challenge_not_found" {
		fmt.Fprintln(os.Stderr, "La richiesta della challenge ha fallito perché non è stata trovata")
		return
	} else if resp1.Transaction.Status == "v4_signed" {
		fmt.Fprintln(os.Stderr, "La transazione è già stata firmata, per cui non è possibile accettarla di nuovo")
		return
	} else if resp1.Transaction.Status != "v4_pending" {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito perché il server ha restituito %s reason %s\n", resp1.Transaction.Status, resp1.Transaction.Reason)
		return
	}

	// Confirm the challenge
	resp2, err := SecureHolderAz(conf, secret, resp1.Transaction, secret.PosteIDCode)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "PIN-ERR-3" {
		fmt.Fprintln(os.Stderr, "Il codice PosteID inserito è errato")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "CERT-ERR-2" {
		fmt.Fprintln(os.Stderr, "Il codice PosteID bloccato per troppi errori")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "v4_challenge_expired" {
		fmt.Fprintln(os.Stderr, "La challenge è scaduta (o non esiste)")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status != "v4_signed" {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito perché il server ha restituito %s reason %s\n", resp2.Status, resp2.Reason)
		return
	}
}

// DenyTransaction deny the specified transaction
func DenyTransaction(conf *config.Configuration, secret *config.Secrets, txid string) {
	// Request the challenge
	resp1, err := SecureHolderChallenge(conf, secret, txid)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta di challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp1.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito perché il server ha restituito %d\n", resp1.StatusCode)
		return
	} else if resp1.Transaction.Status == "v4_error" && resp1.Transaction.Reason == "v4_invalid_request_challenge_not_found" {
		fmt.Fprintln(os.Stderr, "La richiesta della challenge ha fallito perché non è stata trovata")
		return
	} else if resp1.Transaction.Status == "v4_signed" {
		fmt.Fprintln(os.Stderr, "La transazione è già stata firmata, per cui non è possibile negarla")
		return
	} else if resp1.Transaction.Status != "v4_pending" {
		fmt.Fprintf(os.Stderr, "La richiesta della challenge ha fallito perché il server ha restituito %s reason %s\n", resp1.Transaction.Status, resp1.Transaction.Reason)
		return
	}

	// Confirm the challenge
	resp2, err := SecureHolderAzDenying(conf, secret, resp1.Transaction)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito. Errore: %s\n", err)
		return
	} else if resp2.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito perché il server ha restituito %d\n", resp2.StatusCode)
		return
	} else if resp2.Status == "v4_error" && resp2.Reason == "v4_challenge_expired" {
		fmt.Fprintln(os.Stderr, "La challenge è scaduta (o non esiste)")
		secret.PosteIDCode = ""
		return
	} else if resp2.Status != "v4_signed" {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito perché il server ha restituito %s ragione %s\n", resp2.Status, resp2.Reason)
		return
	}
}

// SecureHolderChallenge request a challenge for the specified txid
// It calls the API https://posteid.poste.it/jod-login-schema/secureholder/v4/challenge
func SecureHolderChallenge(config *config.Configuration, secret *config.Secrets, txid string) (SecureHolderV4ChallengeResponse, error) {
	var outResp SecureHolderV4ChallengeResponse
	endpoint := "https://posteid.poste.it/jod-login-schema/secureholder/v4/challenge"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"jti":           txid,
		"appRegisterID": config.AppRegisterID,
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp.Transaction); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// SecureHolderAz confirm the specified challenge
// It calls the API https://posteid.poste.it/jod-login-schema/secureholder/v4/azs
func SecureHolderAz(config *config.Configuration, secret *config.Secrets, challenge TransactionChallenge, posteIDPin string) (SecureHolderV4AzResponse, error) {
	var outResp SecureHolderV4AzResponse
	endpoint := "https://posteid.poste.it/jod-login-schema/secureholder/v4/az"
	signature := SignChallenge(secret.SecretAppToken, posteIDPin, challenge)
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"appRegisterID": config.AppRegisterID,
		"signature": utils.ToJSONWithIdent(map[string]interface{}{
			"jti":       challenge.Jti,
			"signature": signature,
			"userpin":   posteIDPin,
			"authzTool": "POSTEID",
		}),
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
		return outResp, err
	}

	return outResp, nil
}

// SecureHolderAzDenying denies the specified challenge
// It calls the API https://posteid.poste.it/jod-login-schema/secureholder/v4/az
func SecureHolderAzDenying(config *config.Configuration, secret *config.Secrets, challenge TransactionChallenge) (SecureHolderV4AzResponse, error) {
	var outResp SecureHolderV4AzResponse
	endpoint := "https://posteid.poste.it/jod-login-schema/secureholder/v4/az"
	jwe := GenJWEPayloadWithKIDOTpSpect("", "login", config, secret, map[string]interface{}{
		"appRegisterID": config.AppRegisterID,
		"signature":     "v4_rejected",
		"jti":           challenge.Jti,
	})

	req := utils.NewRequestOrPanic("POST", endpoint, strings.NewReader(jwe))
	req.Header.Set("Content-Type", "text/plain; charset=utf-8")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if outResp.StatusCode == 200 {
		if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

func SecureHolderDeletePosteID(config *config.Configuration, secret *config.Secrets) (SecureHolderDeletePosteIDResponse, error) {
	var outResp SecureHolderDeletePosteIDResponse
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/secureholder/v4/native/delete-posteid"

	req := utils.NewRequestOrPanic("POST", endpoint, nil)
	req.Header.Set("Authorization", "Bearer "+secret.AccessToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if outResp.StatusCode == 200 {
		if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
			return outResp, err
		}
	}

	return outResp, nil
}

func SecureHolderNativeProfile(config *config.Configuration, secret *config.Secrets) (SecureHolderNativeProfileResponseResponse, error) {
	var outResp SecureHolderNativeProfileResponseResponse
	endpoint := "https://posteid.poste.it/jod-securelogin-schema/secureholder/v4/native/profile"

	req := utils.NewRequestOrPanic("POST", endpoint, nil)
	req.Header.Set("Authorization", "Bearer "+secret.AccessToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return outResp, err
	}

	ShowHTTPResponseIfDebug(config, resp)
	outResp.StatusCode = resp.StatusCode
	outResp.Cookies = resp.Cookies()

	if outResp.StatusCode == 200 {
		if err = json.NewDecoder(resp.Body).Decode(&outResp); err != nil {
			return outResp, err
		}

		token, err := jwt.ParseSigned(outResp.Profile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "La decodifica del profilo ha fallito. Errore: %s\n", err)
			return outResp, err
		}

		err = token.UnsafeClaimsWithoutVerification(&outResp.ProfileData)
		if err != nil {
			fmt.Fprintf(os.Stderr, "La decodifica del profilo ha fallito. Errore: %s\n", err)
			return outResp, err
		}
	}

	return outResp, nil
}

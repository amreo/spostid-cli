// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package logic

import (
	"crypto/rsa"
	"net/http"
)

type CommandHeader struct {
	CommandResult            string `json:"command-result"`
	CommandResultReason      string `json:"command-result-reason"`
	CommandResultDescription string `json:"command-result-description"`
	CommandResultDetails     string `json:"command-result-details"`
}

type CommandResult struct {
	CommandSuccess      bool        `json:"command-success"`
	CommandErrorCode    string      `json:"command-error-code"`
	CommandErrorMessage string      `json:"command-error-message"`
	CommandResultType   string      `json:"command-result-type"`
	CommandResult       interface{} `json:"command-result"` //sometimes is a string, other times is a {}...
}

type TransactionChallenge struct {
	Jti                    string `json:"jti"`
	Status                 string `json:"status"`
	RandK                  string `json:"randK"`
	TransactionChallenge   string `json:"transaction-challenge"`
	ClusterID              string `json:"clusterID"`
	TransactionDescription struct {
		Title      string `json:"title"`
		AccessType string `json:"accesstype"`
		Service    string `json:"service"`
		Time       int    `json:"time"`
		Level      string `json:"level"`
		IsMobile   bool   `json:"isMobile"`
	} `json:"transaction-description"`
	Reason string `json:"reason"`
}

type TransactionRequest struct {
	Tid            string `json:"tid"`
	AppID          string `json:"appid"`
	Status         string `json:"status"`
	Type           string `json:"type"`
	AppData        string `json:"appdata"`
	AppDataDecoded struct {
		TransactionDescription struct {
			Title      string `json:"title"`
			AccessType string `json:"accesstype"`
			Service    string `json:"service"`
			Time       int    `json:"time"`
			Level      string `json:"level"`
			IsMobile   bool   `json:"isMobile"`
		} `json:"transaction-description"`
	}
	CreateTime int    `json:"createtime"`
	CreateDate string `json:"createdate"`
}

type CommonResponseData struct {
	StatusCode int
	Cookies    []*http.Cookie
}

type AppRegistryRegisterInitResponse struct {
	CommonResponseData
	Data struct {
		PubServerKey string `json:"pubServerKey"`
	}
	PubKey    *rsa.PublicKey
	PubKeyPEM string
}

type AppRegistryRegisterResponse struct {
	CommonResponseData
	Data struct {
		AppUUID      string `json:"app-uuid"`
		OtpSecretKey string `json:"otpSecretKey"`
	} `json:"data"`
}

type AppRegistryActivationResponse struct {
	CommonResponseData
}

type AppRegistryAppConfigResponse struct {
	CommonResponseData
	Header CommandHeader `json:"header"`
	Body   struct {
		Config struct {
			PasswordRules struct {
				Rules []string `json:"regole"`
			} `json:"regolePassword"`
			PasswordChangeErrors map[string]string `json:"erroriCambioPassword"`
			Version              string            `json:"version"`
			TokenIntervalSeconds string            `json:"intervalloTokenSecondi"`
		} `json:"config"`
	} `json:"body"`
}

type AppRegistryAppCheckResponse struct {
	CommonResponseData
	Header CommandHeader `json:"header"`
}

type SecureLoginXMobileAuthJWT0Response struct {
	CommonResponseData
	Pi  string
	Pid string
}

type SecureLoginXMobileAuthJWT3Response struct {
	CommonResponseData
	Tel string
}

type SecureLoginXMobileAuthJWT2Response struct {
	CommonResponseData
	Pi   string
	Pid  string
	Data struct {
		RegisterToken string `json:"token"`
	} `json:"data"`
}

type SecureHolderRegisterAppResponse struct {
	CommonResponseData
	CommandResult
	Data struct {
		AppRegisterID string `json:"appRegisterID"`
		SecretApp     string `json:"secretAPP"`
	} `json:"data"`
}

type SecureHolderCheckRegisterAppResponse struct {
	CommonResponseData
	CommandResult
	Valid bool
}

type SecureHolderNativeChallengeResponse struct {
	CommonResponseData
	Transaction TransactionChallenge
}

type SecureHolderNativeAzResponse struct {
	CommonResponseData
	Status       string `json:"status"`
	ProfileToken string `json:"profile_token"`
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	Reason       string `json:"reason"`
}

type ListTransactionsResponse struct {
	CommonResponseData
	Status      string `json:"status"`
	Reason      string `json:"reason"`
	Transaction struct {
		History []TransactionRequest `json:"history"`
		Pending []TransactionRequest `json:"pending"`
	} `json:"transaction"`
}

type SecureHolderV4ChallengeResponse struct {
	CommonResponseData
	Transaction TransactionChallenge
}

type SecureHolderV4AzResponse struct {
	CommonResponseData
	Status string `json:"status"`
	Reason string `json:"reason"`
}

type SecureHolderDeletePosteIDResponse struct {
	CommonResponseData
	Status string `json:"status"`
	Reason string `json:"reason"`
}

type SecureHolderNativeProfileResponseResponse struct {
	CommonResponseData
	Status      string `json:"status"`
	Reason      string `json:"reason"`
	Profile     string `json:"profile"`
	ProfileData struct {
		Claims struct {
			Address        string `json:"address"`
			Alias          string `json:"alias"`
			CountryOfBirth string `json:"countyOfBirth"`
			DateOfBirth    string `json:"dateOfBirth"`
			DigitalAddress string `json:"digitalAddress"`
			Document       struct {
				DateOfExpire  string `json:"dateOfExpire"`
				DateOfIssued  string `json:"dateOfIssued"`
				Issued        string `json:"issued"`
				Number        string `json:"number"`
				PlaceOfIssued string `json:"placeOfIssued"`
				Type          string `json:"type"`
			} `json:"document"`

			Email            string `json:"email"`
			FamilyName       string `json:"familyName"`
			Name             string `json:"name"`
			FiscalNumber     string `json:"fiscalNumber"`
			FiscalNumberETSI string `json:"fiscalNumberETSI"`
			Gender           string `json:"gender"`
			MobilePhone      string `json:"mobilePhone"`
			NationOfBirth    string `json:"nationOfBirth"`
			PlaceOfBirth     string `json:"placeOfBirth"`
			SpidCode         string `json:"spidCode"`
			Tools            []struct {
				Active bool   `json:"active"`
				Hold   bool   `json:"hold"`
				Key    string `json:"key"`
				Level  int    `json:"level"`
			} `json:"tools"`
		} `json:"claims"`
	}
}

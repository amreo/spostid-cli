package utils

import (
	"bufio"
	"fmt"
	"os"

	"golang.org/x/term"
)

var termState *term.State

// SaveTermState saves the state of the terminal
func SaveTermState() {
	oldState, err := term.GetState(int(os.Stdin.Fd()))
	PanicOrNoErr(err)
	termState = oldState
}

// RestoreTermState restores the state of the terminal
func RestoreTermState() {
	if termState != nil {
		err := term.Restore(int(os.Stdin.Fd()), termState)
		PanicOrNoErr(err)
	}
}

// AskString prompts the user a string with a prompt
// prompt is the string to be written before it is asked to the user to insert a string
func AskString(prompt string) string {
	RestoreTermState()
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(prompt)
	str, err := reader.ReadString('\n')
	PanicOrNoErr(err)
	return str
}

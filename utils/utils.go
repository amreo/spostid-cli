// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package utils

import (
	"bytes"
	cr "crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base32"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"

	"github.com/jltorresm/otpgo"
	"github.com/jltorresm/otpgo/config"
)

// ToJSONReader returns a new reader that reads val as a json. If it fails to marshal, it panics
// val is the input value to be read
func ToJSONReader(val interface{}) io.Reader {
	b, err := json.Marshal(val)
	PanicOrNoErr(err)

	return bytes.NewReader(b)
}

// ToJSON returns the JSON rappresentation of val. If it fails to marshal, it panics
// val is the input value to be converted to json
func ToJSON(val interface{}) string {
	b, err := json.Marshal(val)
	PanicOrNoErr(err)

	return string(b)
}

// ToJSONWithIdent returns the idented-JSON rappresentation of val. If it fails to marshal, it panics
// val is the input value to be converted to json
func ToJSONWithIdent(val interface{}) string {
	b, err := json.MarshalIndent(val, "", " ")
	PanicOrNoErr(err)

	return string(b)
}

// DumpAsJSON prints the content of val as JSON. If it fails to marshal, it panics
// val is the input value to be dumped
func DumpAsJSON(val interface{}) {
	fmt.Println(ToJSON(val))
}

// DumpHTTPResponseWithBodyString dump the http response to the stdout for debugging purpose
// resp is the http response to be dumped
// body is the content of the body to be dumped as body
func DumpHTTPResponseWithBodyString(resp http.Response, body string) {
	fmt.Println("=== Response (header) ===")
	fmt.Printf("Request method: %s\n", resp.Request.Method)
	fmt.Printf("Request path: %s\n", resp.Request.URL.Path)
	fmt.Printf("Status code: %d\n", resp.StatusCode)
	for k, v := range resp.Header {
		fmt.Printf("%s: %s\n", k, v)
	}
	fmt.Println(body)
	fmt.Println("=== End response ===")
}

// DumpHTTPResponse dump the http response to the stdout for debugging purpose. If it fails to read, it panics
// resp is the http response to be dumped
func DumpHTTPResponse(resp http.Response) {
	DumpHTTPResponseWithBodyString(resp, IOReaderToString(resp.Body))
}

// IOReaderToString reads the content from the reader and returns it as string. If it fails to read, it panics
// reader is the reader to be read
func IOReaderToString(reader io.Reader) string {
	b, err := ioutil.ReadAll(reader)
	PanicOrNoErr(err)
	return string(b)
}

// StripFirstLastLine remove the first and the last line of str and returns the string in the middle
// str is the string to be stripped
func StripFirstLastLine(str string) string {
	return str[strings.Index(str, "\n")+1 : strings.LastIndex(strings.TrimSpace(str), "\n")]
}

// PEM2RSAPubKey unmarshal the the RSA public key in the PEM format to *rsa.PublicKey, or panic
// pubKeyPEM is the PEM of the RSA public key
func PEM2RSAPubKey(pubKeyPEM string) *rsa.PublicKey {
	decodedPubKey, rest := pem.Decode([]byte(pubKeyPEM))
	if len(rest) > 0 {
		panic("There is something wrong in pub key decode")
	}

	pubKeyRSA, err := x509.ParsePKIXPublicKey(decodedPubKey.Bytes)
	PanicOrNoErr(err)

	return pubKeyRSA.(*rsa.PublicKey)
}

// PEM2RSAPrivKey unmarshal the the RSA private key in the PEM format to *rsa.PrivateKey, or panic
// privKeyPEM is the PEM of the RSA private key
func PEM2RSAPrivKey(privKeyPEM string) *rsa.PrivateKey {
	decodedPrivKey, rest := pem.Decode([]byte(privKeyPEM))
	if len(rest) > 0 {
		panic("There is something wrong in priv key decode")
	}

	privKeyRSA, err := x509.ParsePKCS1PrivateKey(decodedPrivKey.Bytes)
	PanicOrNoErr(err)

	return privKeyRSA
}

// RSAPubKey2PEM marshal the the RSA public key from the PEM format to *rsa.PublicKey, or panic
// pubKeyPEM is the RSA public key
func RSAPubKey2PEM(pubKey *rsa.PublicKey) string {
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(pubKey)
	PanicOrNoErr(err)

	publicKeyBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyBytes,
	}
	pubKeyPEMBytes := pem.EncodeToMemory(publicKeyBlock)

	return string(pubKeyPEMBytes)
}

// HOTP generates a HMAC-SHA1 HOTP token with eight digits with a int31-random counter and returns both, or panic
// secretKey is the base32 key used for HOTP
// The first returned value is the token, the second returned value is the counter/moving factor
func HOTP(secretKey string) (string, int) {
	hotp := otpgo.HOTP{Key: secretKey, Length: 8, Algorithm: config.HmacSHA1}
	counter := rand.Int31()
	hotp.Counter = uint64(counter)

	token, err := hotp.Generate()
	PanicOrNoErr(err)

	return token, int(counter)
}

// TOTP generates a HMAC-SHA1 TOTP token with six digits and returns it, or panic
// secretKey is the key to be base32-encoded used for TOTP
func TOTP(secretKey string, tokenInterval int) string {
	totp := otpgo.TOTP{Key: base32.StdEncoding.EncodeToString([]byte(secretKey)), Length: 6, Algorithm: config.HmacSHA1, Period: tokenInterval}

	token, err := totp.Generate()
	PanicOrNoErr(err)

	return token
}

// GenerateRSAKeyPair generates a RSA key pair and return its, or panic
// The first returned value is the private key,
// the second returned value is the private ky in PEM format,
// and the third returned value is the public key in PEM format
func GenerateRSAKeyPair() (*rsa.PrivateKey, string, string) {
	// generate key
	privatekey, err := rsa.GenerateKey(cr.Reader, 2048)
	PanicOrNoErr(err)
	publickey := &privatekey.PublicKey

	// convert the private key to PEM
	var privateKeyBytes []byte = x509.MarshalPKCS1PrivateKey(privatekey)
	privateKeyBlock := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: privateKeyBytes,
	}
	privKeyPEMBytes := pem.EncodeToMemory(privateKeyBlock)

	// convert the public key to PEM
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(publickey)
	PanicOrNoErr(err)
	publicKeyBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyBytes,
	}
	pubKeyPEMBytes := pem.EncodeToMemory(publicKeyBlock)

	// return the values
	return privatekey, string(privKeyPEMBytes), string(pubKeyPEMBytes)
}

// AddCookiesToRequest add the cookies to req
// req is the request where the cookies are added
// cookies are the cookies to be added
func AddCookiesToRequest(req *http.Request, cookies []*http.Cookie) {
	for _, v := range cookies {
		req.AddCookie(v)
	}
}

// NewRequest returns a new request with the specified parameters or panics
func NewRequestOrPanic(method string, url string, reader io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, reader)
	PanicOrNoErr(err)

	return req
}

// PanicOrNoErr panics if err is not nil, otherwise it does nothing
func PanicOrNoErr(err error) {
	if err != nil {
		panic(err)
	}
}

func Base64Decode(str string) (string, error) {
	raw, err := base64.RawURLEncoding.DecodeString(str)
	return string(raw), err
}

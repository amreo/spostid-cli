// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"amreo/spostid-cli/config"
	"amreo/spostid-cli/logic"
	"amreo/spostid-cli/utils"
	"crypto/tls"
	"encoding/base32"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strings"
	"syscall"
	"time"

	"github.com/c-bata/go-prompt"
	"golang.org/x/term"
	"gopkg.in/square/go-jose.v2/jwt"
)

// commandRunner is a type used for any function that executes a command
type commandRunner struct {
	execFunc    func([]string, *stateInfo)
	description string
}

// COMMANDS is a lookup table contains the list of the commands with their runner
var COMMANDS = map[string]commandRunner{
	"exit":               {execFunc: runExit, description: "Esce da spostid-cli"},
	"help":               {execFunc: runHelp, description: "Visualizza il messaggio di aiuto"},
	"status":             {execFunc: runShowStatus, description: "Mostra lo stato di spostid-cli"},
	"toggle-debug-http":  {execFunc: runToggleDebugHTTP, description: "Inverte la flag che mostra i messaggi http scambiati"},
	"show-config":        {execFunc: runShowConfig, description: "Mostra la configurazione di spostid-cli"},
	"show-secrets":       {execFunc: runShowSecret, description: "Mostra i segreti di spostid-cli"},
	"clean":              {execFunc: runClean, description: "Resetta la configurazione di spostid-cli"},
	"save":               {execFunc: runSave, description: "Salva la configurazione di spostid-cli"},
	"load":               {execFunc: runLoad, description: "Carica la configurazione di spostid-cli"},
	"set-proxy":          {execFunc: runSetProxy, description: "Imposta il proxy usato da spostid-cli"},
	"regenerate-xdevice": {execFunc: runRegenerateXDevice, description: "Cambia l'identificativo usato da spostid-cli per identificarsi ai server delle Poste"},
	"init":               {execFunc: runInitSession, description: "Inizializza il client"},
	"update":             {execFunc: runUpdateSession, description: "Aggiorna la sesisone del client"},
	"login":              {execFunc: runLogin, description: "Fa il login utente ai server di PosteID"},
	"check":              {execFunc: runCheck, description: "Controlla la sessione"},
	"totp":               {execFunc: runTOTP, description: "Mostra l'ultimo TOTP"},
	"export-totp":        {execFunc: runExportTOTP, description: "Esporta i parametri di configurazione TOTP"},
	"super-login":        {execFunc: runSuperLogin, description: "Esegue il super-login dell'utente chiedendo il codice PosteID"},
	"list-tx":            {execFunc: runListTransactions, description: "Elenca le transazioni/richieste di autorizzazione in corso"},
	"accept-tx":          {execFunc: runAcceptTransaction, description: "Accetta la transazione specificato, o l'ultima se non specificata"},
	"deny-tx":            {execFunc: runDenyTransaction, description: "Rifiuta la transazione specificato, o l'ultima se non specificata"},
	"show-profile":       {execFunc: runShowProfile, description: "Mostra il profilo dell'utente"},
	"show-full-profile":  {execFunc: runShowFullProfile, description: "Mostra tutto il profilo dell'utente"},
	"ask-posteid-code":   {execFunc: runAskPosteIDCode, description: "Chiede il codice posteid"},
	"delete-posteid":     {execFunc: runDeletePosteID, description: "Cancella il codice posteid"},
	"toggle-autosave":    {execFunc: runToggleAutosave, description: "Inverte la presenza di autosalvataggio"},
}

type stateInfo struct {
	Config  config.Configuration
	Secrets config.Secrets
}

// saveStateIfAutosave the state if autosave is enabled
func (state *stateInfo) saveStateIfAutosave() {
	if !state.Config.NotAutoSave {
		state.Config.Save()
		state.Secrets.Save()
	}
}

func main() {
	var state stateInfo
	utils.SaveTermState()
	defer runExit([]string{}, &state)
	runLoad([]string{}, &state)
	if state.Config.XDevice == "" {
		runRegenerateXDevice([]string{}, &state)
	}

	p := prompt.New(
		func(cmd string) {
			cmd = strings.TrimSpace(cmd)
			handleCommand(cmd, &state)
		},
		completer,
		prompt.OptionAddKeyBind(prompt.KeyBind{
			Key: prompt.ControlC,
			Fn: func(_ *prompt.Buffer) {
				runExit([]string{}, &state)
			},
		}),
		prompt.OptionAddKeyBind(prompt.KeyBind{
			Key: prompt.ControlD,
			Fn: func(_ *prompt.Buffer) {
				runExit([]string{}, &state)
			},
		}),
		prompt.OptionPrefix("spostid> "),
		prompt.OptionLivePrefix(func() (string, bool) {
			switch state.Config.Status {
			case config.NotInitialized:
				return "spostid[?]> ", true
			case config.Initialized:
				return "spostid[-]> ", true
			case config.Logged:
				return "spostid[$]> ", true
			case config.SuperLogged:
				return "spostid[#]> ", true
			default:
				return "spostid[?]> ", true
			}
		}),
	)

	p.Run()
}

// completer returns the suggested prompts given the input document
func completer(in prompt.Document) []prompt.Suggest {
	if in.FindStartOfPreviousWord() != 0 {
		return []prompt.Suggest{}
	}

	var s []prompt.Suggest = []prompt.Suggest{}

	for k, v := range COMMANDS {
		s = append(s, prompt.Suggest{
			Text:        k,
			Description: v.description,
		})
	}

	return prompt.FilterHasPrefix(s, in.GetWordBeforeCursor(), false)
}

// handleCommand runs the command specified in the param
// It may call os.Exit
func handleCommand(cmd string, state *stateInfo) {
	parts := strings.Split(cmd, " ")

	if len(parts) > 0 {
		if runner, ok := COMMANDS[parts[0]]; ok {
			runner.execFunc(parts[1:], state)
		} else if len(cmd) != 0 {
			fmt.Println("Comando non trovato. Scrivi `help` per la lista dei comandi")
		}
	}
}

// runExit quit the program with the status code 0
func runExit(arg []string, state *stateInfo) {
	state.saveStateIfAutosave()
	utils.RestoreTermState()
	os.Exit(0)
}

// runHelp prints the help message
func runHelp(arg []string, state *stateInfo) {
	fmt.Println(
		`help: scrive questo messaggio
exit: esce dal programma
status: mostra lo stato
toggle-debug-http: inverte lo stato di debug delle richieste/risposte HTTP
show-config: mostra la configurazione
show-secret: mostra i segreti
clean: cancella tutti i dati delle sessioni
save: salva sia la configurazione che i segreti
load: carica la configurazione e i segreti
set-proxy [URL proxy]: imposta il proxy da usare. Non specificare il proxy per disabilitarlo. 
	Allo stesso tempo il comando abilitata/disabilita la verifica SSL
regenerate-xdevice [modello dispositivo]: rigenera un nuovo xdevice
init: inizializza la sessione a livello di client
update: aggiorna la sessione a livello di client
login: esegue il login dell'utente
check: controlla la sessione dell'utente
totp: genera un codice TOTP
export-totp: mostra i parametri di configurazione per TOTP
super-login: esegue il super login dell'utente
list-tx: lista le richieste di autenticazione storiche e in attesa
accept-tx [id transazione]: accetta una richiesta di autenticazione
deny-tx [id transazione]: rifiuta una richiesta di autenticazione
show-profile: mostra il profilo/dati dell'utente
show-full-profile: mostra l'intero profilo dell'utente
delete-posteid: cancella il codice posteid. NON DISINSCRIVE AL SERVIZIO`)
}

// runShowStatus shows the status of the client
func runShowStatus(arg []string, state *stateInfo) {
	switch state.Config.Status {
	case config.NotInitialized:
		fmt.Println("The client is not initialized")
	case config.Initialized:
		fmt.Println("The client is initialized")
	case config.Logged:
		fmt.Println("The client is logged in")
	case config.SuperLogged:
		fmt.Println("The client is super logged in")
	}
}

// runToggleDebugHTTP toggles the debug flag for http
func runToggleDebugHTTP(arg []string, state *stateInfo) {
	state.Config.DebugHTTP = !state.Config.DebugHTTP
}

// runShowConfig shows the configuration
func runShowConfig(arg []string, state *stateInfo) {
	state.Config.DumpConfiguration()
}

// runShowSecret shows the secrets
func runShowSecret(arg []string, state *stateInfo) {
	state.Secrets.DumpSecrets()
}

// runClean cleans all session data
func runClean(arg []string, state *stateInfo) {
	state.Config.CleanSessionData()
	state.Secrets.CleanSessionData()
	state.saveStateIfAutosave()
}

// runSave saves the configuration and the secrets
func runSave(arg []string, state *stateInfo) {
	state.Config.Save()
	state.Secrets.Save()
}

// runLoad loads the configuration and the secrets
func runLoad(arg []string, state *stateInfo) {
	state.Config = config.LoadConfiguration()
	state.Secrets = config.LoadSecrets()

	newHttpTransport := http.Transport{}
	if state.Config.ProxyURL != "" {
		proxyURL, err := url.Parse(state.Config.ProxyURL)
		if err != nil {
			fmt.Fprintf(os.Stderr, "L'indirizzo proxy fornito non va bene; %v", err)
		} else {
			newHttpTransport.Proxy = http.ProxyURL(proxyURL)
		}
	}
	newHttpTransport.TLSClientConfig = &tls.Config{}
	newHttpTransport.TLSClientConfig.InsecureSkipVerify = state.Config.NoVerifySSL

	http.DefaultClient.Transport = &newHttpTransport
}

func runSetProxy(arg []string, state *stateInfo) {
	if len(arg) > 0 {
		proxyURL, err := url.Parse(arg[0])
		if err != nil {
			fmt.Fprintf(os.Stderr, "L'indirizzo proxy fornito non va bene; %v", err)
			return
		}
		http.DefaultClient.Transport = &http.Transport{
			Proxy: http.ProxyURL(proxyURL),
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}

		state.Config.ProxyURL = proxyURL.String()
		state.Config.NoVerifySSL = true
	} else {
		http.DefaultClient.Transport = &http.Transport{}
		state.Config.ProxyURL = ""
		state.Config.NoVerifySSL = false
	}

	state.saveStateIfAutosave()
}

func runRegenerateXDevice(arg []string, state *stateInfo) {
	rnd := rand.New(rand.NewSource(time.Now().UnixMicro()))
	if len(arg) > 0 {
		state.Config.XDevice = fmt.Sprintf("%08X%03X::Android:12:%s:4.5.916:false", rnd.Uint64(), rnd.Intn(0xFFFFFF), arg[0])
	} else {
		state.Config.XDevice = fmt.Sprintf("%08X%03X::Android:12:ONE A2003:4.5.916:false", rnd.Uint64(), rnd.Intn(0xFFFFFF))
	}
	state.saveStateIfAutosave()
}

func runInitSession(arg []string, state *stateInfo) {
	logic.InitSession(&state.Config, &state.Secrets)
	state.saveStateIfAutosave()
}

func runUpdateSession(arg []string, state *stateInfo) {
	logic.UpdateSession(&state.Config, &state.Secrets)
	state.saveStateIfAutosave()
}

func runLogin(arg []string, state *stateInfo) {
	logic.Login(&state.Config, &state.Secrets)
	state.saveStateIfAutosave()
}

func runCheck(arg []string, state *stateInfo) {
	// Check the configuration status, and if it is not initialized, return
	if state.Config.Status < config.Initialized {
		fmt.Fprintf(os.Stderr, "Devi aver già inizializzato spostid-cli per fare il controllo.\n Esegui il comando init")
		return
	}

	resp, err := logic.SecureHolderCheckRegisterApp(&state.Config, &state.Secrets)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Il controllo della sessione ha fallito. Errore: %s\n", err)
		fmt.Println("OK: false")
		return
	} else if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "Il controllo della sessione ha fallito perché il server ha restituito %d\n", resp.StatusCode)
		fmt.Println("OK: false")
		return
	} else if !resp.CommandSuccess {
		fmt.Fprintf(os.Stderr, "Il controllo della sessione ha fallito. Errore: %s\n", logic.GenErrorFromCmdResult(resp.CommandResult))
		fmt.Println("OK: false")
		return
	}
	fmt.Printf("OK: %v\n", resp.Valid)
}

func runTOTP(arg []string, state *stateInfo) {
	fmt.Println(utils.TOTP(state.Secrets.SecretAppToken, state.Config.UserTOTPInterval))
}

func runExportTOTP(arg []string, state *stateInfo) {
	fmt.Println("Segreto (base32, key): ", base32.StdEncoding.EncodeToString([]byte(state.Secrets.SecretAppToken)))
	fmt.Println("Numero cifre: 6")
	fmt.Println("Intervallo: ", state.Config.UserTOTPInterval)
}

func runSuperLogin(arg []string, state *stateInfo) {
	logic.SuperLogin(&state.Config, &state.Secrets)
}

func runListTransactions(arg []string, state *stateInfo) {
	// Check the configuration status, and if it is not initialized, return
	if state.Config.Status < config.SuperLogged {
		fmt.Fprintln(os.Stderr, "Devi aver essere super-loggato per vedere la lista delle transazioni. Esegui `super-login`")
		return
	}

	txl, err := logic.ListTransactions(&state.Config, &state.Secrets)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito. Errore: %s\n", err)
		return
	} else if txl.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito perché il server ha restituito %d\n", txl.StatusCode)
		return
	} else if txl.Status != "v4_success" {
		fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito con status %s, ragione %s\n", txl.Status, txl.Reason)
		return
	}

	fmt.Println("Transazioni storiche:")
	for _, tx := range txl.Transaction.History {
		fmt.Printf("\t- ID: %s\n\t  Stato: %s\n\t  Tipo: %s (%s L%s)\n\t  Data creazione: %s\n\t  Titolo: %s\n\t  Servizio: %s\n",
			tx.Tid, tx.Status, tx.Type, tx.AppDataDecoded.TransactionDescription.AccessType,
			tx.AppDataDecoded.TransactionDescription.Level, tx.CreateDate, tx.AppDataDecoded.TransactionDescription.Title,
			tx.AppDataDecoded.TransactionDescription.Service)
	}

	fmt.Println("Transazioni pendenti:")
	for _, tx := range txl.Transaction.Pending {
		fmt.Printf("\t- ID: %s\n\t  Stato: %s\n\t  Tipo: %s (%s L%s)\n\t  Data creazione: %s\n\t  Titolo: %s\n\t  Servizio: %s\n",
			tx.Tid, tx.Status, tx.Type, tx.AppDataDecoded.TransactionDescription.AccessType,
			tx.AppDataDecoded.TransactionDescription.Level, tx.CreateDate, tx.AppDataDecoded.TransactionDescription.Title,
			tx.AppDataDecoded.TransactionDescription.Service)
	}
}

func runAcceptTransaction(arg []string, state *stateInfo) {
	if len(arg) > 0 {
		logic.AcceptTransaction(&state.Config, &state.Secrets, arg[0])
	} else {
		// Check the configuration status, and if it is not initialized, return
		if state.Config.Status < config.SuperLogged {
			runSuperLogin(arg, state)
		}

		txl, err := logic.ListTransactions(&state.Config, &state.Secrets)
		if err != nil {
			fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito. Errore: %s\n", err)
			return
		} else if txl.StatusCode != 200 {
			fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito perché il server ha restituito %d\n", txl.StatusCode)
			return
		} else if txl.Status != "v4_success" {
			fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito con status %s, ragione %s\n", txl.Status, txl.Reason)
			return
		}

		if len(txl.Transaction.Pending) > 0 {
			logic.AcceptTransaction(&state.Config, &state.Secrets, txl.Transaction.Pending[0].Tid)
		} else {
			fmt.Fprintf(os.Stderr, "Ehi, non hai nessuna richiesta di autorizzazione in attesa da accettare")
			return
		}
	}
}

func runDenyTransaction(arg []string, state *stateInfo) {
	if len(arg) > 0 {
		logic.DenyTransaction(&state.Config, &state.Secrets, arg[0])
	} else {
		txl, err := logic.ListTransactions(&state.Config, &state.Secrets)
		if err != nil {
			fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito. Errore: %s\n", err)
			return
		} else if txl.StatusCode != 200 {
			fmt.Fprintf(os.Stderr, "La richiesta della lista delle transazioni ha fallito perché il server ha restituito %d\n", txl.StatusCode)
			return
		} else if txl.Status != "v4_success" {
			fmt.Fprintf(os.Stderr, "La firma della challenge ha fallito con status %s, ragione %s\n", txl.Status, txl.Reason)
			return
		}

		if len(txl.Transaction.Pending) > 0 {
			logic.DenyTransaction(&state.Config, &state.Secrets, txl.Transaction.Pending[0].Tid)
		}
	}
}

func runShowProfile(arg []string, state *stateInfo) {
	token, err := jwt.ParseSigned(state.Config.ProfileToken)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La decodifica del profilo ha fallito. Errore: %s\n", err)
		return
	}

	var data struct {
		Claims struct {
			Alias        string `json:"alias"`
			FamilyName   string `json:"familyName"`
			Name         string `json:"name"`
			FiscalNumber string `json:"fiscalNumber"`
		} `json:"claims"`
	}

	err = token.UnsafeClaimsWithoutVerification(&data)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La decodifica del profilo ha fallito. Errore: %s\n", err)
		return
	}

	fmt.Printf(`Username: %s
Nome: %s
Cognome: %s
Codice Fiscale: %s
`, data.Claims.Alias, data.Claims.Name, data.Claims.FamilyName, data.Claims.FiscalNumber)
}

func runShowFullProfile(arg []string, state *stateInfo) {
	// Check the configuration status, and if it is not initialized, return
	if state.Config.Status < config.SuperLogged {
		fmt.Fprintln(os.Stderr, "Devi aver essere super-loggato per vedere la lista delle transazioni. Esegui `super-login`")
		return
	}

	resp, err := logic.SecureHolderNativeProfile(&state.Config, &state.Secrets)
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'ottenimento delle informazione del profilo ha fallito. Errore: %s\n", err)
		return
	} else if resp.StatusCode == 401 {
		fmt.Fprintln(os.Stderr, "L'ottenimento delle informazione del profilo ha fallito perché la sessione è invalida")
		return
	} else if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "L'ottenimento delle informazione del profilo ha fallito perché il server ha restituito %d\n", resp.StatusCode)
		return
	} else if resp.Status != "v4_success" {
		fmt.Fprintf(os.Stderr, "L'ottenimento delle informazione del profilo con status %s, ragione %s\n", resp.Status, resp.Reason)
		return
	}

	fmt.Println("Profilo spid:")
	fmt.Printf(`	Codice identificativo: %s
	Username: %s
`, resp.ProfileData.Claims.SpidCode, resp.ProfileData.Claims.Alias)
	fmt.Println("Modalità login attive:")
	for _, v := range resp.ProfileData.Claims.Tools {
		fmt.Printf(`	- Tipo: %s
	  Attivo: %v
	  Hold: %v,
	  Livello: %d
`, v.Key, v.Active, v.Hold, v.Level)
	}
	fmt.Println("Dati utente:")
	fmt.Printf(`	Nome: %s
	Cognome: %s
	Sesso: %s
	Data di nascita: %s
	Luogo di nascita: %s
	Provincia: %s
	Nazione: %s
	Documento: %s
	Codice fiscale: %s
	Codice fiscale ETSI: %s
`, resp.ProfileData.Claims.Name, resp.ProfileData.Claims.FamilyName, resp.ProfileData.Claims.Gender,
		resp.ProfileData.Claims.DateOfBirth, resp.ProfileData.Claims.PlaceOfBirth,
		resp.ProfileData.Claims.CountryOfBirth, resp.ProfileData.Claims.NationOfBirth,
		resp.ProfileData.Claims.Document.Type, resp.ProfileData.Claims.FiscalNumber,
		resp.ProfileData.Claims.FiscalNumberETSI)

	fmt.Println("Contatti:")
	fmt.Printf(`	Email: %s
	Numero di cellulare: %s
	Domicilio:  %s
	Domicilio digitale (???): %s
`, resp.ProfileData.Claims.Email, resp.ProfileData.Claims.MobilePhone,
		resp.ProfileData.Claims.Address, resp.ProfileData.Claims.DigitalAddress)
	fmt.Println("Documento identificativo:")
	fmt.Printf(`	Tipo di documento: %s
	Documento emesso da: %s
	Numero documento:  %s
	Data emissione documento: %s
	Luogo emissione: %s
	Data scadenza docuemnto: %s
`, resp.ProfileData.Claims.Document.Type, resp.ProfileData.Claims.Document.Issued,
		resp.ProfileData.Claims.Document.Number, resp.ProfileData.Claims.Document.DateOfIssued,
		resp.ProfileData.Claims.Document.PlaceOfIssued, resp.ProfileData.Claims.Document.DateOfExpire)

}

func runAskPosteIDCode(arg []string, state *stateInfo) {
	fmt.Print("Inserire il codice posteid: ")
	bytePosteIDCode, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		fmt.Fprintf(os.Stderr, "L'inserimento del codice posteid ha fallito. Errore: %s\n", err)
		return
	}
	fmt.Println()
	state.Secrets.PosteIDCode = strings.TrimSpace(string(bytePosteIDCode))
}

func runDeletePosteID(arg []string, state *stateInfo) {
	resp, err := logic.SecureHolderDeletePosteID(&state.Config, &state.Secrets)
	if err != nil {
		fmt.Fprintf(os.Stderr, "La cancellazione del codice posteid ha fallito. Errore: %s\n", err)
		return
	} else if resp.StatusCode == 401 {
		fmt.Fprintln(os.Stderr, "La cancellazione del codice posteid ha fallito perché la sessione è invalida")
		return
	} else if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "La cancellazione del codice posteid ha fallito perché il server ha restituito %d\n", resp.StatusCode)
		return
	} else if resp.Status != "v4_success" {
		fmt.Fprintf(os.Stderr, "La cancellazione del codice posteid con status %s, ragione %s\n", resp.Status, resp.Reason)
		return
	}
	state.saveStateIfAutosave()
}

// runToggleAutosave toggles the autosave
func runToggleAutosave(arg []string, state *stateInfo) {
	state.Config.NotAutoSave = !state.Config.NotAutoSave
	state.saveStateIfAutosave()
}

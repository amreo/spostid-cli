<!--
SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>

SPDX-License-Identifier: CC0-1.0
-->

# spostid-cli
Spostid-cli è un client per il [servizio PosteID](https://posteid.poste.it/) delle Poste Italiane che permette di utilizzare il servizio ad esempio per fare login tramite lo SPID delle Poste oppure accettare/rifiutare le richieste di autenticazione. La differenza con l'omonimo client ufficiale è che è software libero e che si usa da linea di comando es scritto in GO.

## Installation
Attualmente non è stato pachettizzato per cui non è prevista una procedura di installazione. 
Per compilarlo è richiesto il pacchetto `go` e basta eseguire
```
go build -o spostid-cli main.go
```

## Usage
Il programma si lancia tranquillamente con `./spostid-cli` e ci si interagisce con i comandi (come una shell).
Il comando più utile è `help` che mostra la lista di tutti i comandi.

### Login
Per fare il login bisogna eseguire i questi comandi e rispondere alle richieste di informazioni;
```
init
login
save
```
Il comando login chiederà l'inserimento di username, password, il codice numerico ricevuto via SMS e eventualmente il codice PosteID, e save salva la configurazione. 

### Autenticazione SPID tramite "notifica in app"
Una volta che si è scelto di fare l'autenticazione tramite "notifica in app" sul portale di login SPID delle poste e cliccato su "notifica in app" bisogna eseguire questi comandi:
```
super-login
accept-tx 
```
Super-login è richiesto solo una volta all'ora. Se il comando `accept-tx` fallisce, probabilmente per sistemarlo basta ripetere con `super-login`, oppure resettare la sessione con `reset` e fare tutto il login.

### Autenticazione SPID tramite codice temporaneo
Una volta che si è scelto di fare l'autenticazione tramite "codice temporaneo" sul portale di login SPID delle poste e cliccato su "codice temporaneo" basta eseguire il comando `totp` per generare un codice da ricopiare sulla casella per il codice e proseguire con il login.

## License
La licenza è GPLv3.
// SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
//
// SPDX-License-Identifier: CC0-1.0

module amreo/spostid-cli

go 1.19

require (
	github.com/OpenPeeDeeP/xdg v1.0.0
	github.com/c-bata/go-prompt v0.2.6
	github.com/google/uuid v1.3.0
	github.com/zalando/go-keyring v0.2.1
)

require (
	github.com/alessio/shellescape v1.4.1 // indirect
	github.com/danieljoos/wincred v1.1.0 // indirect
	github.com/godbus/dbus/v5 v5.0.6 // indirect
	github.com/jltorresm/otpgo v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/pkg/term v1.2.0-beta.2 // indirect
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e // indirect
	golang.org/x/crypto v0.3.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/term v0.2.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
